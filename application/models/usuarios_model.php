<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class Usuarios_Model extends CI_Model
	{
		public function salva($usuario)
		{
			$this->db->insert("usuarios", $usuario);
		}
		
		public function buscaPorEmailESenha($email, $senha)
		{
			$this->db->where("email", $email);
			$this->db->where("senha", md5($senha));
			$usuario = $this->db->get("usuarios")->row_array();
			return $usuario;
		}
	}