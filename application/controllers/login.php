<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Login extends CI_Controller
	{
		public function autentica()
		{
			$email = $this->input->post("email");
			$senha = $this->input->post("senha");
			
			$this->load->model("usuarios_model");
			$usuario = $this->usuarios_model->buscaPorEmailESenha($email, $senha);
			
			if($usuario != null)
			{
				$this->session->set_userdata("usuario_logado", $usuario);
				$this->session->set_flashdata("success", "Usu�rio logado com sucesso.");
			}
			else
			{
				$this->session->set_flashdata("danger", "Usu�rio ou senha inv�lidos.");
			}

			redirect("/");
		}
		
		public function logout()
		{
			$this->session->unset_userdata("usuario_logado");
			$this->session->set_flashdata("success", "Usu�rio deslogado com sucesso.");
			redirect("/");
		}
	}