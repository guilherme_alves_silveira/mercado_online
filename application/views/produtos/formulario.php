<html>
	<head>
		<meta charset="utf-8">
		<link href="<?= base_url("css/bootstrap.css"); ?>" rel="stylesheet" />
	</head>
	<body>
		<div class="container">
			<h1>Cadastrar Novo Produto</h1>
			<?php 
				echo form_open("produtos/novo");
				
				echo form_label("Nome", "nome");
				echo form_input
					(
						array
						(
							"name" => "nome",
							"class" => "form-control",
							"id" => "nome",
							"maxlength" => "255"
						)
					);
				
				echo form_label("Descri��o", "descricao");
				echo form_textarea
					(
						array
						(
							"name" => "descricao",
							"class" => "form-control",
							"id" => "descricao"
						)
					);
				
				echo form_label("Pre�o", "preco");
				echo form_input
					(
						array
						(
							"name" => "preco",
							"class" => "form-control",
							"id" => "preco",
							"type" => "number"
						)
					);
				
				echo form_button
					(
						array
						(
							"content" => "Cadastrar",
							"class" => "btn btn-primary btn-lg",
							"id" => "cadastrar",
							"type" => "submit"
						)
					);
				
				
				echo form_close();
			?>
		</div>
	</body>
</html>