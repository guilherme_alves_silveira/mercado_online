<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="<?= base_url("css/bootstrap.css"); ?>" rel="stylesheet" />
	</head>
	<body>
		<div class="container">
		
			<?php if($this->session->flashdata("success")) : ?>
				<p class="alert alert-success"><?=$this->session->flashdata("success")?></p>
			<?php endif; ?>
			
			<?php if($this->session->flashdata("danger")) : ?>
				<p class="alert alert-danger"><?=$this->session->flashdata("danger")?></p>
			<?php endif; ?>
			
			<h1>Produtos</h1>
			<table class="table">
				<?php 
					foreach($produtos as $produto) 
					{
				?>
						<tr>
							<td><?=$produto["nome"];?></td>
							<td><?=numeroEmReais($produto["preco"]);?></td>
						</tr>
				<?php
					}
				?>
			</table>
			<?php echo anchor("produtos/formulario", "Cadastrar novo produto", array("title" => "Cadastrar novo produto")); ?>
			
			<?php 	

				if($this->session->userdata("usuario_logado"))
				{
					echo anchor("login/logout","Logout", array("class" => "btn btn-primary"));
				}
				else
				{
			?>
			
				<h1>Login</h1>
				<?php 
					echo form_open("login/autentica");
				
					echo form_label("E-mail", "email");
					echo form_input
					(
						array
						(
							"name" => "email",
							"id" => "email",
							"class" => "form-control",
							"maxlength" => "255"
						)
					);
					
					echo form_label("Senha", "senha");
					echo form_password
					(
						array
						(
							"name" => "senha",
							"id" => "senha",
							"class" => "form-control",
							"maxlength" => "255"
						)
					);
					
					echo form_button
					(
						array
						(
							"content" => "Logar",
							"class" => "btn btn-primary btn-lg",
							"id" => "logar",
							"type" => "submit"
						)
					);
					
					echo form_close();
				}
				?>
			
			<h1>Cadastro de Usuários</h1>
			<?php 
				echo form_open("usuarios/novo");
				
				echo form_label("Nome", "nome");
				echo form_input
				(
					array
					(
						"name" => "nome",
						"id" => "nome",
						"class" => "form-control",
						"maxlength" => "255"
					)
				);
				
				echo form_label("E-mail", "email");
				echo form_input
				(
					array
					(
						"name" => "email",
						"id" => "email",
						"class" => "form-control",
						"maxlength" => "255"
					)
				);
				
				echo form_label("Senha", "senha");
				echo form_password
				(
					array
					(
						"name" => "senha",
						"id" => "senha",
						"class" => "form-control",
						"maxlength" => "255"
					)
				);
				
				echo form_button
				(
					array
					(
						"content" => "Cadastrar",
						"class" => "btn btn-primary btn-lg",
						"id" => "cadastrar",
						"type" => "submit"
					)
				);
				
				echo form_close();
			?>
		</div>
	</body>
</html>